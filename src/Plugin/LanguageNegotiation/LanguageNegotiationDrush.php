<?php

namespace Drupal\drush_language_negotiation\Plugin\LanguageNegotiation;

use Drupal\language\LanguageNegotiationMethodBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Swap language to default language for Drush.
 *
 * Drush has a bug where it can't define the correct language thus setting it
 * via this way.
 *
 * @LanguageNegotiation(
 *   id = \Drupal\drush_language_negotiation\Plugin\LanguageNegotiation\LanguageNegotiationDrush::METHOD_ID,
 *   weight = -99,
 *   name = @Translation("Drush Language Switching to correct language"),
 *   description = @Translation("Switch to correct language using drush (Default language instead of ENG)"),
 * )
 */
class LanguageNegotiationDrush extends LanguageNegotiationMethodBase {

  /**
   * The language negotiation method id.
   */
  const METHOD_ID = 'language-drush';

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    $langcode = NULL;

    // If going via Drush.
    if (PHP_SAPI === 'cli') {
      $langcode = \Drupal::languageManager()->getDefaultLanguage()->getId();
    }

    return $langcode;
  }

}
